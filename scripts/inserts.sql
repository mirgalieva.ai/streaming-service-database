INSERT INTO track(track_name, track_duration)
VALUES ('Blank Space', 212),
       ('Shake It Off', 219),
       ('Bad Blood', 211),
       ('...Ready For It', 208),
       ('Don''t Blame Me', 236),
       ('Look What You Made Me Do', 211),
       ('Don''t Threaten Me with a Good Time', 213),
       ('Emperor''s New Clothes', 158),
       ('House of Memories', 208),
       ('ME!', 193),
       ('Hello', 295),
       ('Crazy in Love', 251),
       ('I''m Not The Only One', 239),
       ('Rolling Stone', 202),
       ('Dark Horse', 215),
       ('Cure For Me', 201),
       ('I Like It', 253),
       ('Praise God', 226);

INSERT INTO performer(performer_name, rating)
VALUES ('Taylor Swift', 998699),
       ('Adele', 951614),
       ('Panic!At The Disco', 898189),
       ('Sam Smith', 1234679),
       ('Hurts', 296765),
       ('Katy Perry', 1598740),
       ('Beyoncé', 591447),
       ('Cardi B', 814965),
       ('AURORA', 871942),
       ('Kanye West', 1790872);

INSERT INTO genre(genre_name)
VALUES ('pop'),
       ('rap'),
       ('techno'),
       ('jazz'),
       ('rock'),
       ('country'),
       ('classical music'),
       ('phonk'),
       ('metal'),
       ('r&b');


INSERT INTO album(album_name, performer_id, genre_id, type, year)
VALUES ('25', 2, 1, 'ordinal', 2015),
       ('Death of a Bachelor', 3, 5, 'ordinal', 2016),
       ('1989', 1, 1, 'ordinal', 2014),
       ('reputation', 1, 1, 'ordinal', 2017),
       ('I''m Not The Only One', 4, 1, 'single', 2014),
       ('Surrender', 5, 1, 'ordinal', 2015),
       ('Dark Horse', 6, 1, 'ordinal', 2013),
       ('Beyoncé I Am...Tour Instrumentals', 7, 1, 'ordinal', 2020),
       ('The Gods We Can Touch', 9, 1, 'ordinal', 2022),
       ('Invasion of Privacy', 8, 2, 'ordinal', 2018),
       ('Donda', 10, 2, 'ordinal', 2021),
       ('Lover', 1, 1, 'ordinal', 2019);

INSERT INTO label(label_name)
VALUES ('Getting Out Our Dreams II'),
       ('Atlantic'),
       ('Decca'),
       ('Capitol'),
       ('Universal Music Operations'),
       ('XL'),
       ('Trinitee Urban'),
       ('Fueled By Ramen'),
       ('Apollo A-1'),
       ('FP');

INSERT INTO tracks_in_album(album_id, track_id, track_pos)
VALUES (1, 11, 1),
       (2, 7, 2),
       (2, 8, 4),
       (2, 9, 10),
       (3, 4, 1),
       (3, 5, 4),
       (3, 6, 6),
       (4, 1, 2),
       (4, 2, 6),
       (4, 3, 8),
       (5, 1, 1),
       (6, 14, 10),
       (7, 15, 6),
       (8, 12, 1),
       (9, 16, 4),
       (10, 17, 7),
       (11, 18, 7),
       (12, 10, 16);

INSERT INTO authors(track_id, lyricist, composer, performer_name)
VALUES (1, 'Shellback', null, 'Taylor Swift'),
       (2, 'Shellback', null, 'Taylor Swift'),
       (3, 'Shellback', null, 'Taylor Swift'),
       (4, 'Shellback', null, 'Taylor Swift'),
       (5, 'Shellback', null, 'Taylor Swift'),
       (6, 'Shellback', null, 'Taylor Swift'),
       (7, 'Ricky Wilson', null, 'Panic! At The Disco'),
       (8, 'Dan Wilson', null, 'Panic! At The Disco'),
       (9, 'Brendon Urie', null, 'Panic! At The Disco'),
       (10, 'Taylor Swift', null, 'Taylor Swift, Brendon Urie'),
       (11, 'Adele Adkins', 'Greg Kurstin', 'Adele'),
       (13, 'Sam Smith', null, 'Sam Smith'),
       (12, null, null, 'Beyoncé'),
       (14, 'Theo Hutchcraft', null, 'Hurts'),
       (15, 'Katty Perry ', null, 'Katty Perry'),
       (16, 'AURORA', 'Magnus Skylstad', 'AURORA'),
       (17, 'Manny Rodriguez', null, 'Cardi B, Bad Bunny, J. Balvin'),
       (18, 'Kanye West', null, 'Kanye West');

INSERT INTO performer_X_label(label_id, performer_id, start_dt, end_dt)
values (1, 10, '01-01-2004', null),
       (2, 8, '01-01-2018', null),
       (3, 9, '01-01-2016', null),
       (4, 7, '01-01-2010', '01-01-2012'),
       (4, 2, '01-01-2015', null),
       (6, 2, '01-01-2008', '01-01-2015'),
       (7, 7, '01-01-2012', null),
       (8, 3, '01-01-2005', null),
       (9, 1, '01-01-2012', null),
       (10, 9, '01-01-2015', '01-01-2016');