CREATE TABLE track
(
    track_id       SERIAL PRIMARY KEY,
    track_name     VARCHAR(255) NOT NULL,
    track_duration INTEGER      NOT NULL
);
CREATE TABLE performer
(
    performer_id   SERIAL PRIMARY KEY,
    performer_name VARCHAR(255) UNIQUE NOT NULL,
    rating         INTEGER             NULL
);
CREATE TABLE genre
(
    genre_id   SERIAL PRIMARY KEY,
    genre_name VARCHAR(255) NOT NULL
);
CREATE TABLE album
(
    album_id     SERIAL PRIMARY KEY,
    album_name   VARCHAR(255) NOT NULL,
    performer_id INTEGER,
    genre_id     INTEGER,
    type         VARCHAR(255) NOT NULL,
    year         INTEGER      NOT NULL,
    FOREIGN KEY (performer_id) REFERENCES performer (performer_id),
    FOREIGN KEY (genre_id) REFERENCES genre (genre_id)
);

CREATE TABLE label
(
    label_id   SERIAL PRIMARY KEY,
    label_name VARCHAR(255) UNIQUE NOT NULL
);
CREATE TABLE tracks_in_album
(
    album_id  INTEGER,
    track_id  INTEGER,
    track_pos INTEGER NOT NULL,
    CONSTRAINT PK_tracks_in_album PRIMARY KEY (album_id, track_id),
    FOREIGN KEY (track_id) REFERENCES track (track_id)
);
CREATE TABLE performer_X_label
(
    label_id     INTEGER,
    performer_id INTEGER,
    start_dt     DATE NOT NULL,
    end_dt       DATE DEFAULT NULL,
    CONSTRAINT PK_performer_label PRIMARY KEY (label_id, performer_id),
    FOREIGN KEY (label_id) REFERENCES label (label_id),
    FOREIGN KEY (performer_id) REFERENCES performer (performer_id)
);
CREATE TABLE authors
(
    track_id       INTEGER PRIMARY KEY,
    lyricist       VARCHAR(255) DEFAULT NULL,
    composer       VARCHAR(255) DEFAULT NULL,
    performer_name VARCHAR(255) NOT NULL,
    FOREIGN KEY (track_id) REFERENCES track (track_id)
);
