--Зашифруем  имена исполнителей
CREATE VIEW performer_view AS
SELECT performer_id, CONCAT(SUBSTR(performer_name, 1, 4), '****') AS performer_name
FROM performer;

--Зашифруем названия треков
CREATE VIEW tracks_in_album_view AS
SELECT album_id, CONCAT('track', '****') AS track_pos
FROM tracks_in_album;

--Создадим view для полной информации об альбомах исполнителя
CREATE VIEW album_summary_by_performer AS
SELECT performer.performer_name, COUNT(album.album_id) AS num_albums,
       AVG(performer.rating) AS rating
FROM performer
LEFT JOIN album ON performer.performer_id = album.performer_id
GROUP BY performer.performer_name;

--Создадим view для полной информации об альбомах каждого жанра
CREATE VIEW track_summary_by_genre AS
SELECT genre.genre_name, COUNT(track.track_id) AS num_tracks, AVG(track.track_duration) AS avg_track_duration
FROM genre
LEFT JOIN album ON genre.genre_id = album.genre_id
LEFT JOIN tracks_in_album ON album.album_id = tracks_in_album.album_id
LEFT JOIN track ON tracks_in_album.track_id = track.track_id
GROUP BY genre.genre_name;

--Создадим view для сопоставления исполнителей и треков из студийных альбомов
CREATE VIEW performers_and_tracks_ordinal_album AS
SELECT performer.performer_name, album.album_name
FROM album
JOIN performer ON album.performer_id = performer.performer_id
WHERE album.type = 'ordinal'
ORDER BY performer.performer_name DESC;

--Создадим view  с длительностями треков каждого жанра
CREATE VIEW genre_track_duration AS
SELECT genre.genre_name, SUM(tracks_in_album.track_pos) AS num_tracks
FROM album
JOIN genre ON album.genre_id = genre.genre_id
JOIN tracks_in_album ON album.album_id = tracks_in_album.album_id
GROUP BY genre.genre_name
ORDER BY num_tracks DESC;
