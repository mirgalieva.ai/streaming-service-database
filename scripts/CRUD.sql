SELECT 'Taylor Swift' as performer, track_name
from authors
         join track t on authors.track_id = t.track_id
where performer_name like '%Taylor Swift%';

SELECT performer_name, (extract(year from start_dt)) as start, (extract(year from end_dt)) as finish
from performer_x_label
         join performer p on performer_x_label.performer_id = p.performer_id;

UPDATE performer_x_label
set end_dt='01-01-2022'
where performer_id = 10;

DELETE
from performer_x_label
where label_id = 1 ;

INSERT INTO track(track_name, track_duration)
values ('Free Animal', 180);

INSERT INTO performer(performer_name, rating)
values ('Miley Cyrus', 13398349);

select performer_name
from performer
where rating = (select max(rating) from performer);

DELETE from performer_x_label where end_dt is null;

UPDATE performer set rating = 1000000 where performer_name='Adele';

SELECT avg(rating) from performer;