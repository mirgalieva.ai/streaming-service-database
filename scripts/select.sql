--Вернуть среднюю продолжительность трека в каждом альбомею, отсортировать по году выхода альбома:
SELECT album_name, AVG(track_duration) OVER (PARTITION BY album_id) AS avg_duration
FROM album
         JOIN tracks_in_album USING (album_id)
         JOIN track USING (track_id)
ORDER BY year;

--Вернуть количество треков и общую продолжительность песен каждого исполнителя, чей рейтинг не меньше 1 000 000, отсортировать по убыванию количества треков:
SELECT performer_name, COUNT(track_id) AS num_tracks, SUM(track_duration) AS total_duration
FROM performer
         JOIN authors USING (performer_name)
         JOIN track USING (track_id)
WHERE rating >= 1000000
GROUP BY performer_name
ORDER BY num_tracks DESC;

--Вернуть количество альбомов для каждого жанра, в котором есть хотя бы 10 треков, отсортировать по убыванию количества альбомов:
SELECT genre_name, COUNT(DISTINCT album_id) AS num_albums
FROM genre
         JOIN album USING (genre_id)
         JOIN tracks_in_album USING (album_id)
GROUP BY genre_name
HAVING COUNT(track_id) >= 10
ORDER BY num_albums DESC;

--Вернуть ранг каждого исполнителя, основанный на общем рейтинге, отсортировать по возрастанию рейтинга: 
SELECT performer_name, RANK() OVER (ORDER BY SUM(rating) DESC) AS rank
FROM performer
GROUP BY performer_name
ORDER BY rank;

--Вернуть количество треков в каждом альбоме для каждого исполнителя, отсортировать по имени исполнителя:
SELECT performer_name, type, COUNT(track_id) AS num_tracks
FROM performer
         JOIN album USING (performer_id)
         JOIN tracks_in_album USING (album_id)
GROUP BY performer_name, type
ORDER BY performer_name;

--Вернуть топ-3 исполнителя с самым высоким рейтингом, а также среднюю продолжительность треков, отсортировать по убыванию рейтинга:  
SELECT performer_name, AVG(track_duration) OVER (PARTITION BY performer_name) AS avg_duration
FROM performer
         JOIN authors USING (performer_name)
         JOIN track USING (track_id)
ORDER BY rating DESC
LIMIT 3;
