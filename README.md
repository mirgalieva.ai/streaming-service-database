# Проект по курсу "Базы данных"


### Выбранная тема

Музыкальный триминговый сервис 

## Проектирование

### Концептуальная модель

[Концептуальная модель](./models/conceptual_model.pdf)

### Логическая модель

[Логическая модель](./models/logical_model.pdf)

### Физическая модель

[Физическая модель](./models/physical_model.pdf)


## Создание и заполнение БД, CRUD

Выбранная СУБД - `PostgreSQL`

Инструкции в языке `SQL` делятся на несколько типов. Прежде всего потребуется `Data Definition Language`: с помощью `create`-запросов мы создаём и описываем отношения, которые присутствуют в базе данных. **[Код](./scripts/ddl_script.sql)**

Далее воспользуемся `Data Manipulation Language`. На этом шаге с помощью `insert`-запросов описанные выше отношения заполняются данными, чтобы в дальнейшем с ними можно было работать. **[Код](./scripts/inserts.sql)**

## Дополнительно

### CRUD

CRUD — акроним, обозначающий четыре базовые функции, используемые при работе с базами данных: создание (create), чтение (read), модификация (update), удаление (delete). Введён Джеймсом Мартином в 1983 году как стандартная классификация функций по манипуляции данными. **[Код](./scripts/crud.sql)**

### Версионность

Таблица `performer_X_label` является версионной, так как исполнитель может заключать и расторгать контракт с лейблом. Внутри таблицы храним дополнительно временной промежуток, внутри которого контракт между лейблом и исполнителем был актуальным. Версионность достигается путём использования `SCD 2`

## Работа с Базой Данных

**[Создание select-запросов](./scripts/select.sql)**

**[Создание представлений(view)](./scripts/view.sql)**